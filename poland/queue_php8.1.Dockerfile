# 1 "Dockerfile.in"
# 1 "<built-in>"
# 1 "<command-line>"
# 31 "<command-line>"
# 1 "/usr/include/stdc-predef.h" 1 3 4
# 32 "<command-line>" 2
# 1 "Dockerfile.in"

FROM php:8.1-alpine

# 1 "../install-packages.docker" 1

RUN apk --update upgrade \
    && apk add --no-cache libzip-dev libxml2-dev libxslt-dev \
    && apk add --no-cache --virtual build-dependencies zip $PHPIZE_DEPS \
    && docker-php-ext-install -j$(grep -c ^processor /proc/cpuinfo 2>/dev/null || 1) mysqli pdo_mysql xml xsl pcntl zip \
    && pecl install redis \
    && docker-php-ext-enable redis \
    && mkdir -p /var/www/medcenter/webapp \
    && apk del build-dependencies \
    && rm -rf /var/lib/apt/lists/*
