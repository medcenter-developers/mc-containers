FROM php:8.1-alpine

RUN apk --update upgrade \
    && apk add --no-cache --virtual icu-dev libxml2-dev nodejs npm $PHPIZE_DEPS \
    && docker-php-ext-install -j$(grep -c ^processor /proc/cpuinfo 2>/dev/null || 1) bcmath xml fileinfo pcntl \
    && pecl install redis \
    && docker-php-ext-enable redis\
    && curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin --filename=composer \
    && rm -rf /var/lib/apt/lists/* \
    && rm -rf /var/cache/apk/*
