FROM bitnami/java:1.8.392-9

COPY pone/SpringBootRestService-1.0.jar /var/www/pone-signer/SpringBootRestService-1.0.jar

ENV JAVA_OPTS="-Xmx512m"

EXPOSE 8080