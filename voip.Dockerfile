FROM php:7.3-alpine
RUN apk add --no-cache --virtual build-dependencies icu-dev libxml2-dev $PHPIZE_DEPS \
    && apk add --no-cache zlib-dev libzip-dev zip\
    && docker-php-ext-install -j$(grep -c ^processor /proc/cpuinfo 2>/dev/null || 1) mysqli pdo_mysql bcmath xml fileinfo pdo sockets zip pcntl opcache zip \
    && pecl install redis \
    && docker-php-ext-enable redis\
    && apk del build-dependencies

# Install Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
