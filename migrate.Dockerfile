FROM php:7.4-alpine3.13

RUN apk --update upgrade \
    && apk add --no-cache --virtual build-dependencies icu-dev libxml2-dev $PHPIZE_DEPS \
    && docker-php-ext-install -j$(grep -c ^processor /proc/cpuinfo 2>/dev/null || 1) mysqli pdo_mysql bcmath xml iconv fileinfo pdo sockets \
    && mkdir -p /var/www/medcenter/webapp \
    && apk del build-dependencies \
    && rm -rf /var/lib/apt/lists/*
