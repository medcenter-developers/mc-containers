# 1 "Dockerfile.in"
# 1 "<built-in>"
# 1 "<command-line>"
# 31 "<command-line>"
# 1 "/usr/include/stdc-predef.h" 1 3 4
# 32 "<command-line>" 2
# 1 "Dockerfile.in"

FROM php:8.1-fpm-alpine

# 1 "../install-packages.docker" 1

RUN apk --update upgrade \
    && apk add --no-cache --virtual build-dependencies icu-dev libxml2-dev zip libzip-dev zlib-dev $PHPIZE_DEPS \
    && docker-php-ext-install -j$(grep -c ^processor /proc/cpuinfo 2>/dev/null || 1) mysqli pdo_mysql bcmath xml fileinfo pdo sockets zip opcache \
    && pecl install redis \
    && docker-php-ext-enable redis\
    && mkdir -p /var/www/medcenter/webapp \
    && apk del build-dependencies \
    && rm -rf /var/lib/apt/lists/*

RUN sed -i "s/access.log/;access.log/g" /usr/local/etc/php-fpm.d/docker.conf

EXPOSE 9000
