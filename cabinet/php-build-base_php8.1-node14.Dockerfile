FROM node:14-alpine AS node

FROM php:8.1-alpine

COPY --from=node /usr/lib /usr/lib
COPY --from=node /usr/local/share /usr/local/share
COPY --from=node /usr/local/lib /usr/local/lib
COPY --from=node /usr/local/include /usr/local/include
COPY --from=node /usr/local/bin /usr/local/bin

RUN apk --update upgrade \
    && apk add --no-cache --virtual icu-dev libxml2-dev $PHPIZE_DEPS \
    && docker-php-ext-install -j$(grep -c ^processor /proc/cpuinfo 2>/dev/null || 1) bcmath xml fileinfo pcntl \
    && pear update-channels \
    && pecl update-channels \
    && pecl install redis \
    && docker-php-ext-enable redis\
    && curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin --filename=composer \
    && rm -rf /var/lib/apt/lists/* \
    && rm -rf /var/cache/apk/*
